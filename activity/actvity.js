db.users.find({$or: [{"firstName": {$regex: 'y', $options: '$i'}}, {"lastName": {$regex: 'y', $options: '$i'}}]})

db.users.find({$and: [{"firstName": {$regex: 'e', $options: '$i'}}, {"isAdmin": true}]})

db.products.find({$and: [{"name": {$regex: 'x', $options: '$i'}}, {"price": {$gte: 50000}}]})

db.products.updateMany({"price": {$lt: 2000}}, {$set: {"isActive": false}})

db.products.deleteMany({"price": {$gt: 20000}})